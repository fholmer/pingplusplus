How to setup environment:
    make install-environment
    source Env/Venv/bin/activate
    make upgrade-venv

How to setup a isolated miniconda environment:
    make install-miniconda-environment
    source Env/Venv/bin/activate
    make upgrade-venv

How to build:
    make build