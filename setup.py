#!/usr/bin/env python3

from setuptools import setup, find_packages
from codecs import open
from os import path
import sys

# here = path.abspath(path.dirname(__file__))
# Get the long description from the README file
# with open(path.join(here, 'README.rst'), encoding='utf-8') as f:
#   long_description = f.read()

setup(
    name='pingplusplus',
    version='1.0.0',
    description='ping logging',
    #long_description=long_description,
    url='https://gitlab.com/fholmer/pingplusplus',
    author='Frode Holmer',
    author_email='fholmer+pingplusplus@gmail.com',
    license='MIT',
    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: End Users/Desktop',
        'Topic :: Home Automation',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
    ],
    keywords='ping network',
    packages=find_packages(exclude=['contrib', 'docs', 'tests', 'logs']),
    install_requires=[
        'prompt_toolkit',
        'Pygments',
        'six',
        'wcwidth'
    ],
    extras_require={
        'view': [
            'dash',
            'dash-renderer',
            'dash-core-components',
            'dash-html-components'
        ],
        'dev': ['pytest', 'pylint'],
    },
    entry_points={
        'console_scripts': [
            'ping++=pingplusplus.main:run',
            'view++=viewplusplus.main:run [view]',
        ],
    },
)
