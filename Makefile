.PHONY: all clean build upload install-miniconda-environment install-environment upgrade-venv set-suid-root unset-suid-root
 
all:
	@echo "  clean:"
	@echo "      remove pycache, dist/, build/ *.egg-info" and spec files
	@echo "      "
	@echo "  build:"
	@echo "      build sdist and bdist_wheel package"
	@echo "      build ping++ and view++ with pyinstaller"
	@echo "      "
	@echo "  upgrade-venv:"
	@echo "      update all packages"
	@echo "      "
	@echo "  restore-venv:"
	@echo "      update all packages to version in requirements-freeze"
	@echo "      "
	@echo "  set-suid-root:"
	@echo "      run python as root to access raw sockets in active venv"
	@echo "      "
	@echo "  unset-suid-root:"
	@echo "      undo set-suid-root"
	@echo "      "

clean:
	find . -maxdepth 2 -name '__pycache__' -exec rm --recursive --force {} +
	find . -maxdepth 2 -iname '*.pyc' -exec rm --force {} +
	find . -maxdepth 2 -iname '*.pyo' -exec rm --force {} +
	find . -maxdepth 1 -iname '*.spec' -exec rm --force {} +
	rm --force --recursive build/
	rm --force --recursive dist/
	rm --force --recursive *.egg-info

DASH_CORE := $(shell find $(VIRTUAL_ENV) -name dash_core_components):dash_core_components
DASH_HTML := $(shell find $(VIRTUAL_ENV) -name dash_html_components):dash_html_components
DASH_RNDR := $(shell find $(VIRTUAL_ENV) -name dash_renderer):dash_renderer
MINICONDA_DIR := ./Env/Miniconda3

build:
	python setup.py sdist
	python setup.py bdist_wheel
	LD_LIBRARY_PATH=$(MINICONDA_DIR)/lib pyinstaller --onefile --name ping++ pingpp.py
	LD_LIBRARY_PATH=$(MINICONDA_DIR)/lib pyinstaller --onefile --add-data "$(DASH_CORE)" --add-data "$(DASH_HTML)" --add-data "$(DASH_RNDR)" --name view++ viewpp.py

upload:
	python -m twine upload dist/pingp*

install-miniconda-environment:
	wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
	sh Miniconda3-latest-Linux-x86_64.sh -b -u -p $(MINICONDA_DIR)
	rm Miniconda3-latest-Linux-x86_64.sh
	$(MINICONDA_DIR)/bin/python -m venv ./Env/Venv

install-environment:
	python3 -m venv ./Env/Venv

upgrade-venv:
	python -m pip install -U pip
	python -m pip install -U -r requirements-dev.txt
	python -m pip install -U -r requirements.txt

freeze-venv:
	python -m pip freeze > requirements-freeze.txt

restore-venv:
	python -m pip install -U requirements-freeze.txt

set-suid-root:
	sudo chown root $(MINICONDA_DIR)/bin/python
	sudo chmod u+s $(MINICONDA_DIR)/bin/python

unset-suid-root:
	sudo chown $(USER) $(MINICONDA_DIR)/bin/python
	sudo chmod u-s $(MINICONDA_DIR)/bin/python
